let scoreCount = 0
let clickPower = 1
let clickPowerRounded = Math.round(clickPower * 10) / 10
let Dps = 0
let amount = 1

let shovelCount = 0
let shovelPrice = 15

//hooks ipv shovels als cursors
let chestCount = 0
let chestPrice = 100

let clickChievo = 0


function addPoint (){
    scoreCount = scoreCount + clickPowerRounded
document.getElementById("score").innerHTML = scoreCount
}

function fadeOut(element, duration, finalOpacity, callback) {
    let opacity = 1

    let elementFadingInterval = window.setInterval(function() {
        opacity-= 50 / duration

        if (opacity <= finalOpacity) {
            clearInterval(elementFadingInterval)
            callback()
        }

        element.style.opacity =opacity
    }, 50)
}

function createNumberOnClicker(event) {
    let clicker = document.getElementById("clicker")

    let clickerOffset = clicker.getBoundingClientRect()
    let position = {
        x: event.pageX - clickerOffset.left,
        y: event.pageY - clickerOffset.top
    }

    let element =document.createElement("div")
    element.textContent = "+" + clickPower
    element.classList.add("number", "unselectable")
    element.style.left = position.x + "px"
    element.style.top = position.y + "px"

    clicker.appendChild(element)

    let momentInterval = window.setInterval(function (){
        if (typeof element == "undefined" && element == null) clearInterval(momentInterval)

        position.y--
        element.style.top = position.y + "px"
    }, 10)

    fadeOut(element, 3000, 0.5, function() {
        element.remove()
    })
}

document.getElementById("clicker").addEventListener("click", function (event) {
    addPoint()

    createNumberOnClicker(event)
}, false)

function updateDps() {
    Dps = shovelCount + chestCount * 5
    document.getElementById("dps").innerHTML = Dps
}

function addHook() {
    if (scoreCount >= shovelPrice) {``
        scoreCount = scoreCount - shovelPrice
        shovelCount = shovelCount + amount
        shovelPrice = Math.round(shovelPrice * 1.15)
        clickPower = clickPower + 1

        updateDps()
        document.getElementById("score").innerHTML = scoreCount
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("shovels").innerHTML = shovelCount
    }
}

function addShovel() {
    if (scoreCount >= shovelPrice) {``
        scoreCount = scoreCount - shovelPrice
        shovelCount = shovelCount + amount
        shovelPrice = Math.round(shovelPrice * 1.15)
        clickPower = clickPower + 1

        updateDps()
        document.getElementById("score").innerHTML = scoreCount
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("shovels").innerHTML = shovelCount
    }
}

function addChest() {
    if (scoreCount >= chestPrice) {``
        scoreCount = scoreCount - chestPrice
        chestCount = chestCount + amount
        chestPrice = Math.round(chestPrice * 1.15)
        clickPower = clickPower + 1

        updateDps()
        document.getElementById("score").innerHTML = scoreCount
        document.getElementById("chestprice").innerHTML = chestPrice
        document.getElementById("chests").innerHTML = chestCount
    }
}

setInterval(function () { //adds doubloons per second
    scoreCount = scoreCount + shovelCount
    scoreCount = scoreCount + chestCount * 5
    document.getElementById("score").innerHTML = scoreCount

    document.title = scoreCount + " Doubloons - Captain Click"
}, 1000) // 1000ms = 1 seconde


