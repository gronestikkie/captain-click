

function saveGame() {
    let gameSave = {
        scoreCount: scoreCount,
        clickPower: clickPower,
        Dps: Dps,
        shovelCount: shovelCount,
        shovelPrice: shovelPrice,
        chestCount: chestCount,
        chestPrice: chestPrice
    };
    localStorage.setItem("gameSave", JSON.stringify(gameSave))
}

document.addEventListener ("keydown", function(event) {
    if (event.ctrlKey && event.which == 83) { //CTRL + S
        event.preventDefault()
        saveGame()
    }
}, false)

setInterval(function () {
    saveGame()
}, 30000) //30000ms = 30 seconds, saved elke 30 seconden
