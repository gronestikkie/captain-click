function loadGame() {
    let savedGame = JSON.parse(localStorage.getItem("gameSave"))
    if (typeof savedGame.scoreCount !== "undefined") scoreCount = savedGame.scoreCount
    if (typeof savedGame.clickPower !== "undefined") clickPower = savedGame.clickPower
    if (typeof savedGame.Dps !== "undefined") Dps = savedGame.Dps
    if (typeof savedGame.shovelCount !== "undefined") shovelCount = savedGame.shovelCount
    if (typeof savedGame.shovelPrice !== "undefined") shovelPrice = savedGame.shovelPrice
    if (typeof savedGame.chestCount !== "undefined") chestCount = savedGame.chestCount
    if (typeof savedGame.chestPrice !== "undefined") chestPrice = savedGame.chestPrice
}

window.onload = function () {
    loadGame()
    updateDps()
    document.getElementById("score").innerHTML = scoreCount
    document.getElementById("shovelprice").innerHTML = shovelPrice
    document.getElementById("shovels").innerHTML = shovelCount
    document.getElementById("score").innerHTML = scoreCount
    document.getElementById("chestprice").innerHTML = chestPrice
    document.getElementById("chests").innerHTML = chestCount
    document.getElementById("score").innerHTML = scoreCount
    document.title = scoreCount + " Doubloons - Captain Click"
}